export default {
  fr: {
    filter: "aucun filtre appliqué | 1 filtre appliqué | {n} filtres appliqués",
  },
  en: {
    filter: "No filter | 1 filter | {n} filters",
  },
};
